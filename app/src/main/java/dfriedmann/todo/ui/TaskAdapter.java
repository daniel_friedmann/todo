package dfriedmann.todo.ui;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import dfriedmann.todo.R;
import dfriedmann.todo.core.Task;
import dfriedmann.todo.core.Taskmanager;
import dfriedmann.todo.core.Taskstate;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private Context context;
    private Taskmanager taskmanager;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public TextView taskName;
        public ImageView taskState;
        public CardView recyclerItem;

        public ViewHolder(View aContainer, Context aContext) {
            super(aContainer);

            context = aContext;
            taskmanager = Taskmanager.getInstance(context);

            recyclerItem = (CardView)aContainer.findViewById(R.id.taskRecyclerItem);
            taskState = (ImageView) aContainer.findViewById(R.id.taskStatus);
            taskName = (TextView) aContainer.findViewById(R.id.taskName);

            aContainer.setOnClickListener(this);
            aContainer.setOnLongClickListener(this);

        }

        private void updateTaskstate(){
            Task task = taskmanager.getTask(getAdapterPosition());
            //Update UI
            switch(task.getState()){
                case ACTIVE:
                    //Call Manager to update task
                    taskmanager.updateTaskState(task, Taskstate.DONE);
                    //Update UI
                    taskState.setImageResource(R.drawable.ic_done);
                    taskName.setPaintFlags(taskName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    Log.i("UpdateTask", "task done");
                    break;
                case DONE:
                    //Call Manager to update task
                    taskmanager.updateTaskState(task, Taskstate.ACTIVE);
                    //Update UI
                    taskState.setImageResource(R.drawable.ic_active);
                    taskName.setPaintFlags(0);
                    Log.i("UpdateTask", "task active");
                    break;
                default:
                    throw new IllegalArgumentException("Error while updating taskstate - no taskstate matches current state");
            }
        }

        private void removeTask(){
            taskmanager.removeTask(taskmanager.getTask(getAdapterPosition()));
            //notify changes
            notifyItemRemoved(getAdapterPosition());
            //inform user
            Toast.makeText(context, "Aufgabe erfolgreich gelöscht", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onClick(View v) {
            updateTaskstate();
        }

        @Override
        public boolean onLongClick(View v) {
            removeTask();
            return true;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_listitem_fragment, parent, false);
        return new ViewHolder(v, parent.getContext());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Task current = taskmanager.getTask(position);
        holder.taskName.setText(current.getName());
        //Check for Taskstate and paint the right icon
        if(current.getState() == Taskstate.ACTIVE){
            holder.taskState.setImageResource(R.drawable.ic_active);
        }
        else if(current.getState() == Taskstate.DONE){
            holder.taskState.setImageResource(R.drawable.ic_done);
        }
    }

    @Override
    public int getItemCount() {
        return taskmanager.getSize();
    }

}