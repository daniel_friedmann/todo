package dfriedmann.todo.ui.views;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;
import java.util.logging.Logger;

import dfriedmann.todo.R;
import dfriedmann.todo.core.DBHandler;
import dfriedmann.todo.core.Taskmanager;
import dfriedmann.todo.ui.TaskAdapter;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get updatet tasklist from DB
        Taskmanager.getInstance(this).updateTaskListFromDB();

        initRecyclerView();
        initNewTaskActionBtn();

    }

    private void initRecyclerView(){
        RecyclerView rv = (RecyclerView) findViewById(R.id.taskRecyclerView);
        rv.setAdapter(new TaskAdapter());
        rv.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initNewTaskActionBtn(){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.newTaskFBtn);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NewTask.class));
            }
        });
    }

}
