package dfriedmann.todo.ui.views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Logger;

import dfriedmann.todo.R;
import dfriedmann.todo.core.DBHandler;
import dfriedmann.todo.core.Task;
import dfriedmann.todo.core.Taskmanager;
import dfriedmann.todo.core.Taskstate;
import dfriedmann.todo.ui.TaskAdapter;

public class NewTask extends BaseActivity {


    public static final int SPEECH_REQUEST_CODE = 100;

    private Button saveButton;
    private EditText taskName;

    private EditText remindDate;
    private EditText remindTime;

    private EditText dueDate;
    private EditText dueTime;

    private Calendar remindCalendar;
    private Calendar dueCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        saveButton = (Button) findViewById(R.id.saveNewTaskBtn);
        taskName = (EditText) findViewById(R.id.editTaskName);

        remindDate = (EditText) findViewById(R.id.editTaskRemindDate);
        remindTime = (EditText) findViewById(R.id.editTaskRemindTime);

        dueDate = (EditText) findViewById(R.id.editTaskDueDate);
        dueTime = (EditText) findViewById(R.id.editTaskDueTime);

        remindCalendar = Calendar.getInstance();
        dueCalendar = Calendar.getInstance();

        initSpeechBtn();

        initRemindDate();
        initRemindTime();

        initDueDate();
        initDueTime();

    }

    /**
     * Called by saveNewTaskBtn to save a new task
     * @param v
     */
    public void setSaveButtonClick(View v){
        //Add new task
        Taskmanager.getInstance(this).addTask(new Task(taskName.getText().toString(),dueCalendar.getTime(),  remindCalendar.getTime()));
        //Jump back to Main
        startActivity(new Intent(this, MainActivity.class));
    }

    private void initSpeechBtn(){
        ImageButton btn = (ImageButton) findViewById(R.id.taskSpeechBtn);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speechInput();
            }
        });
    }

    private void speechInput(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        try{
            startActivityForResult(i, SPEECH_REQUEST_CODE);
        }
        catch (ActivityNotFoundException e){
            Logger.getAnonymousLogger().info("Speechinput not supported");
        }
    }

    private void initRemindTime(){
        remindTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar currentTime = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(NewTask.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        Calendar myCalendar = getTimeCalendar(hourOfDay, minute);
                        String timeString = DateUtils.formatDateTime(NewTask.this, myCalendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                        remindTime.setText(timeString);

                        remindCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.get(Calendar.HOUR_OF_DAY));
                        remindCalendar.set(Calendar.MINUTE, myCalendar.get(Calendar.MINUTE));
                    }

                }
                        , currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), DateFormat.is24HourFormat(getApplicationContext()));

                dialog.show();

            }
        });
    }

    private void initRemindDate(){
        remindDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(NewTask.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar myCalendar = getDateCalendar(year, monthOfYear, dayOfMonth);

                        String dateString = DateUtils.formatDateTime(NewTask.this, myCalendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE);
                        remindDate.setText(dateString);

                        remindCalendar.set(Calendar.YEAR, myCalendar.get(Calendar.YEAR));
                        remindCalendar.set(Calendar.MONTH, myCalendar.get(Calendar.MONTH));
                        remindCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH));

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

                dialog.show();

            }
        });
    }

    private void initDueDate(){
        dueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(NewTask.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar myCalendar = getDateCalendar(year, monthOfYear, dayOfMonth);

                        String dateString = DateUtils.formatDateTime(NewTask.this, myCalendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE);
                        dueDate.setText(dateString);

                        dueCalendar.set(Calendar.YEAR, myCalendar.get(Calendar.YEAR));
                        dueCalendar.set(Calendar.MONTH, myCalendar.get(Calendar.MONTH));
                        dueCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH));

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

                dialog.show();

            }
        });
    }

    private void initDueTime(){
        dueTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar currentTime = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(NewTask.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        Calendar myCalendar = getTimeCalendar(hourOfDay, minute);
                        String timeString = DateUtils.formatDateTime(NewTask.this, myCalendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                        dueTime.setText(timeString);

                        dueCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.get(Calendar.HOUR_OF_DAY));
                        dueCalendar.set(Calendar.MINUTE, myCalendar.get(Calendar.MINUTE));
                    }

                }
                        , currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), DateFormat.is24HourFormat(getApplicationContext()));

                dialog.show();

            }
        });
    }

    private Calendar getTimeCalendar(int hourOfDay, int minute){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        return c;
    }

    private Calendar getDateCalendar(int year, int monthOfYear, int dayOfMonth){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, monthOfYear);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        return c;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK){
            taskName.setText(taskName.getText() + data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
        }
    }

}
