package dfriedmann.todo.core;

import java.util.Date;

/**
 * Created by Daniel on 20.04.2016.
 * Simple DAO for task
 */
public class Task {


    private int id;
    private String name;
    private Date dueDate;
    private Date remindDate;
    private Taskstate state;


    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date aDueDate) {
        this.dueDate = aDueDate;
    }

    public Date getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(Date remindDate) {
        this.remindDate = remindDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Taskstate getState() {
        return state;
    }

    public void setState(Taskstate state) {
        this.state = state;
    }

    public Task(String aName, Date aDueDate,  Date aRemindDate) {
        this(aName, aDueDate, aRemindDate, Taskstate.ACTIVE);
    }

    public Task(String aName, Date aDueDate,  Date aRemindDate, Taskstate aState){
        setName(aName);
        setDueDate(aDueDate);
        setState(aState);
        setRemindDate(aRemindDate);
    }

    public Task(int aId, String aName, Date aDueDate,  Date aRemindDate, Taskstate aState){
        this(aName, aDueDate, aRemindDate, aState);
        setId(aId);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dueDate=" + dueDate +
                ", remindDate=" + remindDate +
                ", status=" + state +
                '}';
    }
}
