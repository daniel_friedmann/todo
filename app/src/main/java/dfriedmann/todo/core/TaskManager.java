package dfriedmann.todo.core;

import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by Daniel on 26.04.2016.
 */
public class Taskmanager {

    private static Taskmanager instance;

    private Context context;

    //does all the DBs operations
    private DBHandler db;

    //manages the task alarms
    private TaskAlarmManager alarmManager;

    //used for holding the taskitems
    private List<Task> taskList;

    private Taskmanager(Context aContext){
        context = aContext;
        db = DBHandler.getInstance(context);
        alarmManager = new TaskAlarmManager();
        updateTaskListFromDB();
    }

    public synchronized static Taskmanager getInstance(Context aContext){

        if(instance == null){
            Taskmanager.instance =  new Taskmanager(aContext);
        }
        return instance;
    }

    /**
     * Gets the tasks from DB
     */
    public void updateTaskListFromDB() {
        taskList = db.fetchAllTasks();
        Log.d("Taskmanager ", "CURRENT LISTE: "+getCurrentTaskList().toString());
    }

    public List<Task> getCurrentTaskList() {
        return taskList;
    }

    public void addTask(Task aTask){
        //Persist to DB
        db.addTask(aTask);

        //Set Alarm if RemindDate is set
        if(aTask.getRemindDate() != null){
            alarmManager.setAlarm(context, aTask.getRemindDate(), aTask.getName(), aTask.getName());
            System.out.println("Alarm set for: "+aTask.getRemindDate());
        }

        //Update current List
        updateTaskListFromDB();
    }

    public void removeTask(Task aTask){
        //Delete from DB
        db.deleteTaskById(aTask.getId());
        //Update current List
        updateTaskListFromDB();
    }

    public void updateTaskState(Task aTask, Taskstate aState){
        aTask.setState(aState);
        db.updateTask(aTask);
    }

    public Task getTask(int aPosition){
        return taskList.get(aPosition);
    }

    public int getSize(){
        return taskList.size();
    }

}