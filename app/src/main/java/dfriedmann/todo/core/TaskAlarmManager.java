package dfriedmann.todo.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import java.util.Date;

import dfriedmann.todo.core.TaskAlarmReceiver;

/**
 * Created by Daniel on 22.07.2016.
 */
public class TaskAlarmManager {

    public void setAlarm(Context aContext, Date time, String aTitle, String aMessage){
        Intent alertIntent = new Intent(aContext, TaskAlarmReceiver.class);
        alertIntent.putExtra("title", aTitle);
        alertIntent.putExtra("message", aMessage);
        AlarmManager alarmManager = (AlarmManager) aContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time.getTime(), PendingIntent.getBroadcast(aContext, 1, alertIntent, PendingIntent.FLAG_UPDATE_CURRENT));
    }


}
