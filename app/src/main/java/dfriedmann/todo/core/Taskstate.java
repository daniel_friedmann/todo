package dfriedmann.todo.core;

/**
 * Created by Daniel on 22.04.2016.
 */
public enum Taskstate {

    ACTIVE(0), DONE(1), TRASH(2);

    private final int value;
    Taskstate(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
