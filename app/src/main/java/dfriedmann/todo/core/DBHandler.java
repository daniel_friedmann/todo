package dfriedmann.todo.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 25.04.2016.
 */
public class DBHandler extends SQLiteOpenHelper {

    private static DBHandler instance;

    private static final int DATABASE_VERSION = 11;
    private static final String DATABASE_NAME = "todo.db";
    private static final String TABLE_TASKS = "tasks";

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TASKNAME = "name";
    private static final String COLUMN_TASKSTATE = "state";
    private static final String COLUMN_DUEDATE = "duedate";
    private static final String COLUMN_REMINDATE = "reminddate";

    public static synchronized DBHandler getInstance(Context context) {

        if (instance == null) {
            instance = new DBHandler(context.getApplicationContext());
        }
        return instance;
    }

    private DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_TASKS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,  " +
                COLUMN_TASKNAME + " TEXT NOT NULL, " +
                COLUMN_TASKSTATE + " INTEGER NOT NULL, " +
                COLUMN_DUEDATE + " TEXT, " +
                COLUMN_REMINDATE + " TEXT " +
                ");";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //Delete the table and recreate it
           db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
           onCreate(db);
    }

    /***
     * Add a Task to the database
     * @param aTask
     */
    public void addTask(Task aTask){
        ContentValues values = new ContentValues();
        values.put(COLUMN_TASKNAME, aTask.getName());
        values.put(COLUMN_TASKSTATE, aTask.getState().getValue());

        if(aTask.getDueDate() != null){
            values.put(COLUMN_DUEDATE,  aTask.getDueDate().getTime());
        }
        if(aTask.getRemindDate() != null){
            values.put(COLUMN_REMINDATE, aTask.getRemindDate().getTime());
        }

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_TASKS, null, values);
        db.close();
    }

    public void deleteTaskById(int aId){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_TASKS +" WHERE " + COLUMN_ID + "=\"" + aId + "\";" );
        db.close();
    }

    public void updateTask(Task aTask){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TASKNAME, aTask.getName());
        values.put(COLUMN_TASKSTATE, aTask.getState().getValue());
        values.put(COLUMN_DUEDATE, aTask.getDueDate().toString());
        values.put(COLUMN_REMINDATE, aTask.getRemindDate().getTime());

        String[] whereArgs = {String.valueOf(aTask.getId())};

        db.update(TABLE_TASKS, values, COLUMN_ID +" =?", whereArgs);

        db.close();
    }

    public List<Task> fetchAllTasks(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_TASKS,null);

        ArrayList<Task> tasks = new ArrayList<>();

        if (cursor .moveToFirst()) {

            while (!cursor.isAfterLast()) {

                //get task information
                int id = cursor.getInt(cursor
                        .getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor
                        .getColumnIndex(COLUMN_TASKNAME));

                Date deadline = new Date(cursor.getLong(cursor
                        .getColumnIndex(COLUMN_DUEDATE)));

                Date remindDate = new Date(cursor.getLong(cursor
                        .getColumnIndex(COLUMN_REMINDATE)));


                int value = cursor.getInt(cursor.getColumnIndex(COLUMN_TASKSTATE));

                Taskstate state = Taskstate.ACTIVE;

                if(value == Taskstate.ACTIVE.getValue()){
                    state = Taskstate.ACTIVE;
                }
                else if(value == Taskstate.DONE.getValue()){
                    state = Taskstate.DONE;
                    System.out.println("JA");
                }

                tasks.add(new Task(id, name, deadline, remindDate, state));
                cursor.moveToNext();
            }
        }

        db.close();

        Log.d("Taskmanager ", "DB LISTE: "+tasks.toString());

        return tasks;
    }

}
