package dfriedmann.todo.core;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import dfriedmann.todo.ui.views.MainActivity;

public class TaskAlarmReceiver extends BroadcastReceiver {

    public static final int NOTIFIC_REQUEST_CODE = 101;

    @Override
    public void onReceive(Context context, Intent intent) {

        String message = intent.getStringExtra("message");
        String title = intent.getStringExtra("title");

        PendingIntent notificIntent = PendingIntent.getActivity(context, NOTIFIC_REQUEST_CODE,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle(title)
                .setContentText(message)
                .setTicker(title);

        nBuilder.setContentIntent(notificIntent);
        nBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        //dismiss notification when clicked on it
        nBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, nBuilder.build());

    }
}